/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader.csv;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormatSymbols;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.cellprocessor.ift.StringCellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

/**
 * CSV BigDecimal parser that improves the suplied one, allowing currency symbols
 *
 * @author Andy Geach
 */
public class BetterParseBigDecimal extends CellProcessorAdaptor implements StringCellProcessor {

    private static final char DEFAULT_DECIMAL_SEPARATOR = '.';

    private final DecimalFormatSymbols symbols;

    /**
     * Constructs a new <tt>ParseBigDecimal</tt> processor, which converts a String to a BigDecimal.
     */
    public BetterParseBigDecimal() {
        this.symbols = null;
    }

    /**
     * Constructs a new <tt>ParseBigDecimal</tt> processor, which converts a String to a BigDecimal using the supplied
     * <tt>DecimalFormatSymbols</tt> object to convert any decimal separator to a "." before creating the BigDecimal.
     *
     * @param symbols the decimal format symbols, containing the decimal separator
     * @throws NullPointerException if symbols is null
     */
    public BetterParseBigDecimal(final DecimalFormatSymbols symbols) {
        super();
        checkPreconditions(symbols);
        this.symbols = symbols;
    }

    /**
     * Checks the preconditions for creating a new ParseBigDecimal processor.
     *
     * @param symbols the decimal format symbols, containing the decimal separator
     * @throws NullPointerException if symbols is null
     */
    private static void checkPreconditions(final DecimalFormatSymbols symbols) {
        if (symbols == null) {
            throw new NullPointerException("symbols should not be null");
        }
    }

    /**
     * {@inheritDoc}
     *
     * @throws org.supercsv.exception.SuperCsvCellProcessorException if value is null, isn't a String, or can't be parsed as a BigDecimal
     */
    @Override
    public Object execute(final Object value, final CsvContext context) {
        validateInputNotNull(value, context);

        final BigDecimal result;
        if (value instanceof String) {
            String strValue = (String) value;
            try {
                if (symbols == null) {
                    result = new BigDecimal(strValue, MathContext.DECIMAL32);
                } else {
                    result = new BigDecimal(fixSymbols(strValue, symbols), MathContext.DECIMAL32);
                }
            } catch (final NumberFormatException e) {
                throw new SuperCsvCellProcessorException(String.format("'%s' could not be parsed as a BigDecimal",
                        value), context, this, e);
            }
        } else {
            throw new SuperCsvCellProcessorException(String.class, value, context, this);
        }

        return next.execute(result, context);
    }

    /**
     * Fixes the symbols in the input String (currently only decimal separator and grouping separator) so that the
     * String can be parsed as a BigDecimal.
     *
     * @param s       the String to fix
     * @param symbols the decimal format symbols
     * @return the fixed String
     */
    private static String fixSymbols(final String s, final DecimalFormatSymbols symbols) {
        final char groupingSeparator = symbols.getGroupingSeparator();
        final char decimalSeparator = symbols.getDecimalSeparator();
        final String currencySymbol = symbols.getCurrencySymbol();
        return s.replace(String.valueOf(groupingSeparator), "").replace(decimalSeparator, DEFAULT_DECIMAL_SEPARATOR).replace(currencySymbol, "").trim();
    }
}
