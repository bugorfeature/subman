/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseEnum;
import org.supercsv.cellprocessor.StrReplace;
import org.supercsv.cellprocessor.Trim;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.cellprocessor.joda.ParseLocalDate;

import net.bugorfeature.subman.loader.csv.BetterParseBigDecimal;
import net.bugorfeature.subman.loader.csv.FrequencyProcessor;
import net.bugorfeature.subman.loader.csv.PaymentMethodProcessor;
import net.bugorfeature.subman.loader.csv.SalutationTypeProcessor;
import net.bugorfeature.subman.loader.csv.SubscriptionTypeProcessor;
import net.bugorfeature.subman.model.Branch;
import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Gender;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.model.Ward;

/**
 * Loads BSECCA data from CSV file
 *
 * @author Andy Geach
 */
@Component
public class BseccaCsvLoader extends DefaultCsvLoader {

    // membAddressee,Gender,membType,membNum,
    // membHouse,membStreet,membVillage,membTown,
    // membCounty,membPostcode,membTelephone,membEmail,
    // membWard,membBranch,"Surname, First Name, Inits. ",Salutation,
    // membFirstName,membInitials,membSurname,membSpouse,
    // subsAmount£,subsFreq,PayMonth , StartDate,
    // subsDateDue, subsDatePaid,subsAmountPaid£,subsPayMethod,
    // subsReminder1, subsCardex,subsStatus,MembMERLIN,
    // membNotes (max 50 characters)

    protected DateTimeFormatter bseccaFormatter = DateTimeFormat.forPattern("dd/MM/yy");


    @Override
    public CellProcessor[] getFieldProcessors() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setCurrencySymbol("£");

        final CellProcessor[] processors = new CellProcessor[] {
            new Optional(), new ParseEnum(Gender.class, true), new SubscriptionTypeProcessor(), new Optional(new Trim()), // 1
            new StrReplace(",$", "", new Trim()), new Optional(new Trim()), new Optional(new Trim()), new Trim(),         // 2
            new Trim(), new Trim(), new Optional(new Trim()), new Optional(new Trim()),                                   // 3
            new Trim(), new Optional(new Trim()), new Optional(), new SalutationTypeProcessor(),                          // 4
            new Trim(), new Optional(new Trim()), new Trim(), new Optional(),                                             // 5
            new BetterParseBigDecimal(symbols), new FrequencyProcessor(), new Optional(), new Optional(),                 // 6
            new ParseLocalDate(bseccaFormatter), new ParseLocalDate(bseccaFormatter), new BetterParseBigDecimal(symbols), new PaymentMethodProcessor(),
            new Optional(), new ParseBool(), new ParseEnum(SubscriptionStatus.class, true), new Optional(),               // 8
            new Optional(new Trim())                                                                                      // 9
        };
        return processors;
    }

    @Override
    public String[] getFieldMappings() {
        return new String[]{
            null, "gender", "subscriptionType", "number",                        // 1
            "address1", "address2", "address3", "townCity",                      // 2
            "county", "postcode", "telephoneNo", "emailAddress",                 // 3
            "customMemberField", "customSubscriptionField", null, "salutation",  // 4
            "firstName", "initials", "lastName", null,                           // 5
            "dueAmount", "frequency", null, null,                                // 6
            "dueDate", "paidDate", "paidAmount", "paymentMethod",                // 7
            null, "cardIssued", "status", null,                                  // 8
            "notes"                                                              // 9
        };
    }

    @Override
    public void doCustomSubscriptionProcessing(Subscription subscription) {
        BigDecimal adjustedDueAmount;
        LocalDate adjustedDueDate;

        switch (subscription.getFrequency()) {
            case BI_ANNUAL:
                adjustedDueAmount = subscription.getDueAmount().divide(BigDecimal.valueOf(2.00), 2, BigDecimal.ROUND_HALF_EVEN);
                adjustedDueDate = adjustMonth(subscription.getDueDate(), -6);
                break;

            case QUARTER:
                adjustedDueAmount = subscription.getDueAmount().divide(BigDecimal.valueOf(4.00), 2, BigDecimal.ROUND_HALF_EVEN);
                adjustedDueDate = adjustMonth(subscription.getDueDate(), -9);
                break;

            case MONTH:
                adjustedDueAmount = subscription.getDueAmount().divide(BigDecimal.valueOf(12.00), 2, BigDecimal.ROUND_HALF_EVEN);
                adjustedDueDate = adjustMonth(subscription.getDueDate(), -11);
                break;

            default:
                adjustedDueAmount = subscription.getDueAmount();
                adjustedDueDate = subscription.getDueDate();

        }
        subscription.setDueAmount(adjustedDueAmount);
        subscription.setDueDate(adjustedDueDate);
        subscription.setCardIssued(Boolean.TRUE); // TODO configurable custom fields

        subscription.setCustomField_1(Branch.fromString(subscription.getCustomField_1()).name()); // TODO configurable custom fields
    }

    /**
     * Set communications preferences
     *
     * @param member
     */
    @Override
    public void doCustomMemberProcessing(Member member) {
        if (StringUtils.hasText(member.getEmailAddress())) {
            member.setCommPrefs(CommunicationPreferencesType.EMAIL);
        } else {
            member.setCommPrefs(CommunicationPreferencesType.POST);
        }

        member.setCustomField_1(Ward.fromString(member.getCustomField_1()).name()); // TODO configurable custom fields
    }

    @Override
    public boolean supports(String name) {
        return "BSECCA".equals(name);
    }

    private LocalDate adjustMonth(LocalDate dueDate, int numberOfMonths) {
        return dueDate.monthOfYear().addToCopy(numberOfMonths);
    }
}
