/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.dozer.DozerBeanMapper;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseEnum;
import org.supercsv.cellprocessor.Trim;
import org.supercsv.cellprocessor.constraint.Unique;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.cellprocessor.joda.ParseLocalDate;
import org.supercsv.exception.SuperCsvException;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Gender;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.PaymentMethod;
import net.bugorfeature.subman.model.SalutationType;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.model.SubscriptionFrequency;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.model.SubscriptionType;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.MemberRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;
import net.bugorfeature.subman.valid.MemberValidator;
import net.bugorfeature.subman.valid.SubscriptionValidator;

/**
 * Default implementation of CsvLoader. Subclasses should need only to override getFieldProcessors(),
 * getFieldMappings(), and optionally doCustomProcessing()
 *
 * @author Andy Geach
 */
@Component
public class DefaultCsvLoader implements CsvLoader {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultCsvLoader.class);

    @Autowired
    protected SubscriptionRepository subscriptionRepository;

    @Autowired
    protected MemberRepository memberRepository;

    @Autowired
    protected CommunityRepository communityRepository;

    @Autowired
    protected SubscriptionValidator subscriptionValidator;

    @Autowired
    protected MemberValidator memberValidator;

    @Autowired
    protected DozerBeanMapper dozerBeanMapper;

    protected DateTimeFormatter formatter = ISODateTimeFormat.date();


    @Override
    public CellProcessor[] getFieldProcessors() {
        return new CellProcessor[]{
                new ParseEnum(SalutationType.class, true), new Trim(), new Trim(), new Optional(new Trim()),
                new ParseEnum(Gender.class, true), new Trim(), new Optional(new Trim()), new Optional(new Trim()),
                new Trim(), new Trim(), new Trim(), new Optional(new Trim()),
                new Optional(new Trim()), new ParseEnum(CommunicationPreferencesType.class, true), new Unique(new Trim()), new ParseBigDecimal(),
                new ParseLocalDate(formatter), new ParseBigDecimal(), new ParseLocalDate(formatter), new ParseEnum(SubscriptionType.class, true),
                new ParseEnum(SubscriptionFrequency.class, true), new ParseEnum(SubscriptionStatus.class, true), new ParseEnum(PaymentMethod.class, true), new ParseBool(),
                new Optional(new Trim()), new Optional(new ParseLocalDate(formatter))
        };
    }

    @Override
    public String[] getFieldMappings() {
        return new String[]{
                "salutation", "firstName", "lastName", "initials",
                "gender", "address1", "address2", "address3",
                "townCity", "county", "postCode", "telephoneNo",
                "emailAddress", "commPrefs", "number", "dueAmount",
                "dueDate", "paidAmount", "paidDate", "subscriptionType",
                "frequency", "status", "paymentMethod", "cardIssued",
                "notes", "memberSince"};
    }

    @Override
    public ImportResult load(InputStream stream, Integer communityId) {

        ImportResult result = new ImportResult();

        Community community = communityRepository.findOne(communityId);
        if (community == null) {
            result.addIssue(-1, -1, "n/a", "No Community found with ID " + communityId);
            return result;
        }

        ICsvBeanReader reader = null;
        String[] columnHeaders = null;

        try {
            reader = new CsvBeanReader(new InputStreamReader(stream), CsvPreference.STANDARD_PREFERENCE);
            columnHeaders = reader.getHeader(true); // ignore the header

        } catch (Exception e) {
            LOG.error("Problem creating CSV reader: " + e);
            result.addIssue(-1, -1, "n/a", "Problem creating stream reader");
            return result;
        }

        ImportObject importObject = ImportObject.EMPTY_INSTANCE;
        do {

            boolean valid = true;

            try {

                importObject = reader.read(ImportObject.class, getFieldMappings(), getFieldProcessors());

            } catch (Exception e) {

                valid = false;
                String message;
                if (e.getMessage().startsWith("this processor does not accept null input")) {
                    message = "Null input is invalid for this column";
                } else {
                    message = e.getMessage();
                }

                LOG.error("Problem reading CSV line: " + e);

                int colIndex = -1;
                if (e instanceof SuperCsvException) {
                    colIndex = ((SuperCsvException) e).getCsvContext().getColumnNumber();

                }
                result.addIssue(reader.getLineNumber(), colIndex, columnHeaders[colIndex - 1], message);
            }

            saveLoadObjects(valid, importObject, community, reader, result);

        } while (importObject != null);


        try {
            LOG.info("Closing");
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                LOG.error("Problem closing CSV reader: " + e);
            }
        }

        return result;
    }

    private void saveLoadObjects(boolean valid, ImportObject importObject, Community community,
                                 ICsvBeanReader reader, ImportResult result) {
        if (valid && importObject != null) {
            try {
                Member member = dozerBeanMapper.map(importObject, Member.class);

                doCustomMemberProcessing(member);
                member = memberRepository.save(member);

                Subscription subscription = dozerBeanMapper.map(importObject, Subscription.class);
                subscription.setCommunity(community);
                subscription.setMember(member);

                doCustomSubscriptionProcessing(subscription);
                subscription = subscriptionRepository.save(subscription);

                if (subscription != null) {
                    result.incrementSuccess();
                }
            } catch (Exception e) {
                LOG.error("Problem saving model objects: " + e, e);
                result.addIssue(reader.getLineNumber(), e.getMessage());
            }
        }
    }

    @Override
    public boolean supports(String name) {
        return false;
    }

    /**
     * Subclasses should override this method for custom processing
     *
     * @param subscription
     */
    public void doCustomSubscriptionProcessing(Subscription subscription) {
        // do nothing
    }

    /**
     * Subclasses should override this method for custom processing
     *
     * @param member
     */
    public void doCustomMemberProcessing(Member member) {
        // do nothing
    }
}
