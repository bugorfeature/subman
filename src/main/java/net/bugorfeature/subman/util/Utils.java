/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.util;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bugorfeature.subman.model.Branch;
import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.PaymentMethod;
import net.bugorfeature.subman.model.SalutationType;
import net.bugorfeature.subman.model.SubscriptionFrequency;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.model.Ward;

/**
 * Utilities
 *
 * @author Andy Geach
 */
public class Utils {

    private static final Logger LOG = LoggerFactory.getLogger(Utils.class);

    private static DateTimeFormatter formatter = ISODateTimeFormat.date(); // TODO options

    public static final Utils INSTANCE = new Utils();

    private Utils() {
        // hide implicit constructor
    }

    public static LocalDate buildDate(String dateText) {
        return formatter.parseLocalDate(dateText);
    }

    public static LocalDate buildDate(String dateText, String format) {
        DateTimeFormatter formatter2 = DateTimeFormat.forPattern(format);
        return formatter2.parseLocalDate(dateText);
    }

    public static void printValidValues() {
        LOG.info("\nBranch:");
        for (Branch branch : Branch.values()) {
            LOG.info(branch.getDescription());
        }

        LOG.info("\nWard:");
        for (Ward value : Ward.values()) {
            LOG.info(value.getDescription());
        }

        LOG.info("\nPaymentMethod:");
        for (PaymentMethod value : PaymentMethod.values()) {
            LOG.info(value.getDescription());
        }

        LOG.info("\nSubscriptionFrequency:");
        for (SubscriptionFrequency value : SubscriptionFrequency.values()) {
            LOG.info(value.getDescription());
        }

        LOG.info("\nSubscriptionStatus:");
        for (SubscriptionStatus value : SubscriptionStatus.values()) {
            LOG.info(value.getDescription());
        }

        LOG.info("\nSalutationType:");
        for (SalutationType value : SalutationType.values()) {
            LOG.info(value.getDescription());
        }

        LOG.info("\nCommunicationPreferencesType:");
        for (CommunicationPreferencesType value : CommunicationPreferencesType.values()) {
            LOG.info(value.getDescription());
        }
    }
}
