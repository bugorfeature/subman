/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.service.CommunityService;
import net.bugorfeature.subman.service.SubscriptionService;

/**
 * Subscriber API controller
 *
 * @author Andy Geach
 */
@RestController()
@RequestMapping("/api")
public class Subscribers {

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private CommunityService communityService;

    @RequestMapping(value = "/subscriberList", method = RequestMethod.GET, produces = "application/json")
    public Iterable<Subscription> listSubscribersForCommunityBetter(HttpSession session) {
        Community current = communityService.getCommunityForId((Integer)session.getAttribute(CommunityController.COMMUNITY_ID_KEY));
        return subscriptionService.getSubscribersForCommunity(current);
    }
}
