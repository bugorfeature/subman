package net.bugorfeature.subman.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.service.CommunityService;
import net.bugorfeature.subman.service.SubscriptionService;

/**
 * Download controller
 *
 * @author Andy Geach
 */
@Controller
@RequestMapping("/doc")
@SessionAttributes({CommunityController.COMMUNITY_OBJ_KEY})
public class DownloadController {

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private CommunityService communityService;

    @RequestMapping(value = "/xls", method = RequestMethod.GET, produces = "application/vnd.ms-excel" )
    protected String xls(HttpSession session, Model model) {

        Community current = communityService.getCommunityForId((Integer)session.getAttribute(
                CommunityController.COMMUNITY_ID_KEY));
        Iterable<Subscription> subs = subscriptionService.getSubscribersForCommunity(current);

        model.addAttribute("subs", subs);
        return "excelView";
    }

    @RequestMapping(value = "/pdf", method = RequestMethod.GET, produces = "application/pdf")
    protected String handleRequestInternal(HttpSession session, Model model) {

        Community current = communityService.getCommunityForId((Integer) session.getAttribute(
                CommunityController.COMMUNITY_ID_KEY));
        Iterable<Subscription> subs = subscriptionService.getSubscribersForCommunity(current);

        model.addAttribute("subs", subs);
        return "pdfView";
    }
}
