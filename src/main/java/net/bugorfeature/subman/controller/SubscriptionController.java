/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.bugorfeature.subman.form.MemberForm;
import net.bugorfeature.subman.form.SubscriptionForm;
import net.bugorfeature.subman.model.Branch;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.PaymentMethod;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.model.SubscriptionFrequency;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.service.MemberService;
import net.bugorfeature.subman.service.SubscriptionService;

import static net.bugorfeature.subman.controller.SubscriptionController.FREQUENCY_TYPES_KEY;
import static net.bugorfeature.subman.controller.SubscriptionController.STATUS_TYPES_KEY;

/**
 * Subscription controller
 *
 * @author Andy Geach
 */
@Controller
@RequestMapping("/subscription")
@SessionAttributes({CommunityController.COMMUNITY_OBJ_KEY, "paymentTypes", FREQUENCY_TYPES_KEY, STATUS_TYPES_KEY, "subscriptionTypes", "branchTypes"})
public class SubscriptionController {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionController.class);
    public static final String FREQUENCY_TYPES_KEY = "frequencyTypes";
    public static final String STATUS_TYPES_KEY = "statusTypes";
    public static final String CREATE_OR_EDIT_SUBSCRIPTION_VIEW_KEY = "createOrEditSubscription";
    public static final String REDIRECT_COMMUNITY_LIST_MEMBERS_VIEW_KEY = "redirect:/community/listMembers";

    @Autowired
    private SubscriptionService service;

    @Autowired
    private MemberService memberService;


    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String setupNew(Model model) {

        model.addAttribute("paymentTypes", PaymentMethod.values());
        model.addAttribute(FREQUENCY_TYPES_KEY, SubscriptionFrequency.values());
        model.addAttribute(STATUS_TYPES_KEY, SubscriptionStatus.values());
        model.addAttribute("branchTypes", Branch.values()); // TODO remove

        return CREATE_OR_EDIT_SUBSCRIPTION_VIEW_KEY;
    }

    @ModelAttribute("subscription")
    public SubscriptionForm addSubscription(HttpSession session) {
        SubscriptionForm result = new SubscriptionForm();
        MemberForm member = (MemberForm)session.getAttribute("newMember");
        if (member != null) {
            result.setFirstName(member.getFirstName());
            result.setLastName(member.getLastName());
        }

        return result;
    }

    @RequestMapping(value = "/edit/{memberId}", method = RequestMethod.GET)
    public String setupEdit(HttpSession session, @PathVariable("memberId") Integer memberId, Model model, RedirectAttributes redirect) {

        Community current = (Community)session.getAttribute(CommunityController.COMMUNITY_OBJ_KEY);
        Subscription subscription = service.getSubscription(memberId, current.getIdNumber());

        if (subscription == null) {
            redirect.addFlashAttribute("warning", String.format("No member found with ID %d", memberId));
            return REDIRECT_COMMUNITY_LIST_MEMBERS_VIEW_KEY;
        }

        SubscriptionForm subscriptionForm = convertFrom(subscription);
        model.addAttribute("subscription", subscriptionForm);
        model.addAttribute("paymentTypes", PaymentMethod.values());
        model.addAttribute(FREQUENCY_TYPES_KEY, SubscriptionFrequency.values());
        model.addAttribute(STATUS_TYPES_KEY, SubscriptionStatus.values());
        model.addAttribute("branchTypes", Branch.values()); // TODO remove

        return CREATE_OR_EDIT_SUBSCRIPTION_VIEW_KEY;
    }

    @RequestMapping(value = "/pay/{memberId}", method = RequestMethod.GET)
    public String setupPay(@PathVariable("memberId") Integer memberId, Model model) {

        Subscription subscription = service.getSubscription(memberId);
        SubscriptionForm subscriptionForm = convertFrom(subscription);
        subscriptionForm.setPaidAmount(subscriptionForm.getDueAmount());
        model.addAttribute("subscription", subscriptionForm);
        model.addAttribute(STATUS_TYPES_KEY, SubscriptionStatus.values());
        model.addAttribute(FREQUENCY_TYPES_KEY, SubscriptionFrequency.values());

        return "addPayment";
    }

    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    public String processUpdate(HttpSession session, @Valid @ModelAttribute("subscription") SubscriptionForm form,
            BindingResult result, SessionStatus status, RedirectAttributes redirect) {

        if (result.hasErrors()) {
            return CREATE_OR_EDIT_SUBSCRIPTION_VIEW_KEY;
        }

        Subscription subscription;

        if (form.getIdNumber() != null) {
            subscription = convertTo(form);
            service.save(subscription);


        } else {
            Community current = (Community)session.getAttribute(CommunityController.COMMUNITY_OBJ_KEY);
            MemberForm member = (MemberForm)session.getAttribute("newMember");
            subscription = buildNew(form, member, current);
            service.save(subscription);
        }

        String message = String.format("Subscription for member '%s %s' saved successfully",
            subscription.getMember().getFirstName(), subscription.getMember().getLastName());
        status.setComplete();
        redirect.addFlashAttribute("message", message);

        return REDIRECT_COMMUNITY_LIST_MEMBERS_VIEW_KEY;
    }

    @RequestMapping(value = "/submitPayment", method = RequestMethod.POST)
    public String processPayment(@Valid @ModelAttribute("subscription") SubscriptionForm form,
                                BindingResult result, SessionStatus status, RedirectAttributes redirect) {

        if (result.hasErrors()) {
            return "addPayment";
        }

        Subscription subscription = convertTo(form);

        service.save(subscription);

        String message = String.format("Payment for member '%s %s' added successfully",
                subscription.getMember().getFirstName(), subscription.getMember().getLastName());
        status.setComplete();

        redirect.addFlashAttribute("message", message);
        return REDIRECT_COMMUNITY_LIST_MEMBERS_VIEW_KEY;
    }

    private Subscription convertTo(SubscriptionForm form) {
        Subscription subscription = service.getSubscription(form.getMemberId());
        try {
            BeanUtils.copyProperties(subscription, form);
        } catch (Exception e) {
            log.error("Problem converting to: " + e);
        }

        return subscription;
    }

    private Subscription buildNew(SubscriptionForm subscriptionForm, MemberForm memberForm, Community current) {
        Subscription subscription = new Subscription();
        Member member = new Member();
        try {
            BeanUtils.copyProperties(subscription, subscriptionForm);
            BeanUtils.copyProperties(member, memberForm);
            member = memberService.save(member);
            subscription.setMember(member);
            subscription.setCommunity(current);
        } catch (Exception e) {
            log.error("Problem building new: " + e);
        }

        return subscription;
    }

    private SubscriptionForm convertFrom(Subscription subscription) {
        SubscriptionForm form = new SubscriptionForm();
        try {
            BeanUtils.copyProperties(form, subscription);
            form.setOriginalDueDate(subscription.getDueDate());
            form.setFirstName(subscription.getMember().getFirstName());
            form.setLastName(subscription.getMember().getLastName());
            form.setMemberId(subscription.getMember().getIdNumber());
        } catch (Exception e) {
            log.error("Problem converting from: " + e);
        }

        return form;
    }
}
