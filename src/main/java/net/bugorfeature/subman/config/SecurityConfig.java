/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Security config
 *
 * @author Andy Geach
 */
@Configuration
public class SecurityConfig {

    @Bean
    public ApplicationSecurity applicationSecurity() {
        return new ApplicationSecurity();
    }

    @Order(1)
    protected static class ApplicationSecurity extends WebSecurityConfigurerAdapter {

        public static final String LOGOUT = "/logout";
        public static final String LOGIN = "/login";

        @Value("${spring.mvc.rememberme.key}")
        private String rememberMeKey;

        @Autowired
        ApplicationContext context;

        @Autowired
        private RememberMeServices rememberMeServices;


        @Override
        protected void configure(HttpSecurity httpSecurity) throws Exception {
            httpSecurity
                .authorizeRequests()
                    .antMatchers("/", LOGIN, LOGOUT, "/error").permitAll()
                    .antMatchers("/manage/beans", "/manage/mappings", "/manage/flyway", "/manage/autoconfig",
                                 "/manage/configprops", "/manage/trace", "/manage/env", "/manage/logfile",
                                 "/manage/metrics").hasAuthority("ADMIN")
                    .antMatchers("/manage/health", "/manage/info").permitAll()
                    .antMatchers("/h2-console/**").hasAuthority("USER")
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage(LOGIN)
                    .failureUrl(LOGIN + "?error")
                    .permitAll()
                    .defaultSuccessUrl("/community/list", true)
                    .and()
                .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher(LOGOUT))
                    .logoutUrl(LOGOUT)
                    .logoutSuccessUrl(LOGIN)
                    .and()
                .rememberMe()
                    .key(rememberMeKey)
                    .rememberMeServices(rememberMeServices);
        }

        @Override
        public void configure(WebSecurity webSecurity) throws Exception {
            webSecurity
                .ignoring()
                    .antMatchers("/css/**", "/images/**", "/js/**", "/fonts/**");
        }
    }
}
