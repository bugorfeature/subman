/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.bugorfeature.subman.loader.BseccaCsvLoader;
import net.bugorfeature.subman.loader.CsvLoader;
import net.bugorfeature.subman.loader.DefaultCsvLoader;

/**
 * Config for CSV loaders
 *
 * @author Andy Geach
 */
@Configuration
public class CsvLoaderConfig {

    @Bean(name = "defaultCsvLoader")
    public CsvLoader defaultLoader() {
        return new DefaultCsvLoader();
    }

    @Bean(name = "bseccaCsvLoader")
    public CsvLoader bseccaLoader() {
        return new BseccaCsvLoader();
    }
}
