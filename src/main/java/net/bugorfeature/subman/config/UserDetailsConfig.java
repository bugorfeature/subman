/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

/**
 * User details manager config
 *
 * @author Andy Geach
 */
@Configuration
public class UserDetailsConfig {

    @Autowired
    private DataSource dataSource;

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager manager = new JdbcUserDetailsManager();
        manager.setDataSource(dataSource);
        manager.setUsersByUsernameQuery("select username,password,enabled from user_table where username = ?");
        manager.setAuthoritiesByUsernameQuery("select username,authority from authority where username = ?");
        manager.setCreateUserSql("insert into user_table (username, password, enabled) values (?,?,?)");
        manager.setDeleteUserSql("delete from user_table where username = ?");
        manager.setUpdateUserSql("update user_table set password = ?, enabled = ? where username = ?");
        manager.setCreateAuthoritySql("insert into authority (username, authority) values (?,?)");
        manager.setDeleteUserAuthoritiesSql("delete from authority where username = ?");
        manager.setUserExistsSql("select username from user_table where username = ?");
        manager.setChangePasswordSql("update user_table set password = ? where username = ?");
        return manager;
    }

}
