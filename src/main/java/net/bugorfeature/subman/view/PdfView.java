/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.view;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bugorfeature.subman.model.Subscription;

/**
 * PDF view
 *
 * @author Andy Geach
 */
public class PdfView extends AbstractPdfView {

    /**
     * Subclasses must implement this method to build an iText PDF document,
     * given the model. Called between {@code Document.open()} and
     * {@code Document.close()} calls.
     * <p>Note that the passed-in HTTP response is just supposed to be used
     * for setting cookies or other HTTP headers. The built PDF document itself
     * will automatically get written to the response after this method returns.
     *
     * @param model    the model Map
     * @param document the iText Document to add elements to
     * @param writer   the PdfWriter to use
     * @param request  in case we need locale etc. Shouldn't look at attributes.
     * @param response in case we need to set cookies. Shouldn't write to it.
     * @throws Exception any exception that occurred during document building
     * @see com.lowagie.text.Document#open()
     * @see com.lowagie.text.Document#close()
     */
    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
        HttpServletRequest request, HttpServletResponse response) throws Exception {

        Iterable<Subscription> subs = (Iterable<Subscription>)model.get("subs");

        for (Subscription sub : subs) {

            document.add(new Paragraph(String.format("[%s] %s %s %s %s %s %s %s %s %s %s",
                    sub.getNumber(),
                    sub.getMember().getFirstName(),
                    sub.getMember().getInitials(),
                    sub.getMember().getLastName(),
                    sub.getMember().getGender().getDescription(),
                    sub.getMember().getAddress1(),
                    sub.getMember().getAddress2(),
                    sub.getMember().getAddress3(),
                    sub.getMember().getTownCity(),
                    sub.getMember().getCounty(),
                    sub.getMember().getPostCode())));
        }
    }
}
