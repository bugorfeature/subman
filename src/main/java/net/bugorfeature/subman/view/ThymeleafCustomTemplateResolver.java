/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.view;

import java.util.Map;

import org.thymeleaf.TemplateProcessingParameters;
import org.thymeleaf.context.VariablesMap;
import org.thymeleaf.templateresolver.TemplateResolution;
import org.thymeleaf.templateresolver.TemplateResolver;
import org.thymeleaf.util.Validate;

/**
 * Extension of the default Thymeleaf TemplateResolver for supporting custom template locations.
 *
 * This class looks up a configurable value from the session, and if a value exists with that key, looks for the template
 * in a subdirectory named with that value. The full path would be comprised of:
 *
 * 'default template directory' / 'looked-up value' / 'original template name'
 *
 * Defaults to original template name in default location.
 *
 * @author Andy Geach
 */
public class ThymeleafCustomTemplateResolver extends TemplateResolver {

    public static final String DEFAULT_SESSION_VAR_NAME = "customTemplateDirectory";

    private String sessionVariableName = DEFAULT_SESSION_VAR_NAME;

    @Override
    public TemplateResolution resolveTemplate(final TemplateProcessingParameters parameters) {

        checkInitialized();

        Validate.notNull(parameters, "Parameters cannot be null");

        if (computeResolvable(parameters)) {
            return buildResolution(adjustForCustomDirectory(parameters));
        } else {
            return buildResolution(parameters);
        }
    }

    /**
     * <p>
     * Computes whether a template can be resolved by this resolver or not,
     * applying the corresponding patterns. <b>Meant only for use by subclasses</b>.
     * </p>
     *
     * @param parameters the template processing parameters
     * @return whether the template is resolvable or not
     */
    @Override
    protected boolean computeResolvable(TemplateProcessingParameters parameters) {
        VariablesMap<String, Object> map = parameters.getProcessingContext().getContext().getVariables();
        Map session = (Map) map.get("session");
        return session.containsKey(sessionVariableName);
    }

    private TemplateProcessingParameters adjustForCustomDirectory(TemplateProcessingParameters original) {

        VariablesMap<String, Object> map = original.getProcessingContext().getContext().getVariables();
        Map session = (Map) map.get("session");

        return new TemplateProcessingParameters(
                original.getConfiguration(),
                session.get(sessionVariableName) + "/" + original.getTemplateName(),
                original.getContext());
    }

    private TemplateResolution buildResolution(TemplateProcessingParameters params) {
        return new TemplateResolution(
                params.getTemplateName(),
                computeResourceName(params),
                computeResourceResolver(params),
                computeCharacterEncoding(params),
                computeTemplateMode(params),
                computeValidity(params));
    }

    public void setSessionVariableName(String sessionVariableName) {
        this.sessionVariableName = sessionVariableName;
    }
}
