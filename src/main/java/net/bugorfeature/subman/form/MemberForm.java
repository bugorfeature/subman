/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.form;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Gender;
import net.bugorfeature.subman.model.SalutationType;

/**
 * Form backing object for Member instances
 *
 * @author Andy Geach
 */
public class MemberForm implements Serializable {

    private static final long serialVersionUID = 302128969707477405L;

    private Integer idNumber;

    @NotNull
    private SalutationType salutation;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    private String initials;

    @NotNull
    private Gender gender;

    @NotBlank
    private String address1;

    private String address2;

    private String address3;

    @NotBlank
    private String townCity;

    @NotBlank
    private String county;

    @NotBlank
    @Size(min=7, max=8)
    private String postCode;

    @Size(max=25)
    private String telephoneNo;

    @Email
    private String emailAddress;

    @NotNull
    private CommunicationPreferencesType commPrefs;

    private String customField_1;

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public SalutationType getSalutation() {
        return salutation;
    }

    public void setSalutation(SalutationType salutation) {
        this.salutation = salutation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getTownCity() {
        return townCity;
    }

    public void setTownCity(String townCity) {
        this.townCity = townCity;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public CommunicationPreferencesType getCommPrefs() {
        return commPrefs;
    }

    public void setCommPrefs(CommunicationPreferencesType commPrefs) {
        this.commPrefs = commPrefs;
    }

    public String getCustomField_1() {
        return customField_1;
    }

    public void setCustomField_1(String customField_1) {
        this.customField_1 = customField_1;
    }
}
