/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.PaymentMethod;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.model.SubscriptionFrequency;
import net.bugorfeature.subman.model.SubscriptionStatus;

import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;

/**
 * Subscription service implementation
 *
 * @author Andy Geach
 */
@Service
public class SubscriptionServiceHandler implements SubscriptionService {

    public static final String VALUE_KEY = "value";
    public static final String COUNT_KEY = "count";
    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    CommunityRepository communityRepository;

    @Override
    public Iterable<Subscription> getSubscribersForCommunity(Community community) {
        return subscriptionRepository.findByCommunity(community);
    }

    @Override
    public Subscription getSubscription(Integer memberId) {
        return subscriptionRepository.findByMemberIdNumber(memberId);
    }

    @Override
    public Subscription getSubscription(Integer memberId, Integer communityId) {
        return subscriptionRepository.findByMemberIdNumberAndCommunityIdNumber(memberId, communityId);
    }

    @Override
    public void save(Subscription subscription) {
        subscriptionRepository.save(subscription);
    }

    @Override
    public List<Map<String, String>> getStatusStats(Community community) {
        List<Map<String, String>> results = new ArrayList<>();
        List<Object[]> raw = subscriptionRepository.getStatusStats(community);
        int total = 0;
        for (Object[] subscription : raw) {

            Map<String, String> map = new HashMap<>();
            map.put(VALUE_KEY, ((SubscriptionStatus) subscription[0]).getDescription());
            map.put(COUNT_KEY, (subscription[1]).toString());
            total += (Long) subscription[1];
            results.add(map);
        }

        addTotal(results, total);

        return results;
    }

    @Override
    public List<Map<String, String>> getFrequencyStats(Community community) {
        List<Map<String, String>> results = new ArrayList<>();
        List<Object[]> raw = subscriptionRepository.getFrequencyStats(community);
        int total = 0;
        for (Object[] subscription : raw) {

            Map<String, String> map = new HashMap<>();
            map.put(VALUE_KEY, ((SubscriptionFrequency) subscription[0]).getDescription());
            map.put(COUNT_KEY, (subscription[1]).toString());
            total += (Long) subscription[1];
            results.add(map);
        }

        addTotal(results, total);

        return results;
    }

    @Override
    public List<Map<String, String>> getPaymentTypeStats(Community community) {
        List<Map<String, String>> results = new ArrayList<>();
        List<Object[]> raw = subscriptionRepository.getPaymentTypeStats(community);
        int total = 0;
        for (Object[] subscription : raw) {

            Map<String, String> map = new HashMap<>();
            map.put(VALUE_KEY, ((PaymentMethod) subscription[0]).getDescription());
            map.put(COUNT_KEY, (subscription[1]).toString());
            total += (Long) subscription[1];
            results.add(map);
        }

        addTotal(results, total);

        return results;
    }

    private void addTotal(List<Map<String, String>> results, int total) {
        Map<String, String> map = new HashMap<>();
        map.put(VALUE_KEY, "Total");
        map.put(COUNT_KEY, Integer.toString(total));
        results.add(map);
    }

    public void setSubscriptionRepository(SubscriptionRepository subscriptionRepository) {
        this.subscriptionRepository = subscriptionRepository;
    }

    public void setCommunityRepository(CommunityRepository communityRepository) {
        this.communityRepository = communityRepository;
    }
}
