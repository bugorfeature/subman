/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.repo.MemberRepository;

/**
 * Membership service implementation
 *
 * @author Andy Geach
 */
@Service
public class MemberServiceHandler implements MemberService {

    @Autowired
    MemberRepository memberRepository;

    @Override
    public Member getMember(Integer memberId, Integer communityId) {
        return memberRepository.findByIdNumberAndSubscriptionsCommunityIdNumber(memberId, communityId);
    }

    @Override
    public Member save(Member member) {
        return memberRepository.save(member);
    }

    public MemberRepository getMemberRepository() {
        return memberRepository;
    }

    public void setMemberRepository(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }
}
