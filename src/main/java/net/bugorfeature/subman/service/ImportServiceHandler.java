/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import net.bugorfeature.subman.loader.CsvLoader;
import net.bugorfeature.subman.loader.DefaultCsvLoader;
import net.bugorfeature.subman.loader.ImportResult;
import net.bugorfeature.subman.loader.ImportRowIssue;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.repo.MemberRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Import service implementation
 *
 * @author Andy Geach
 */
@Service
public class ImportServiceHandler implements ImportService {

    private static final Logger log = LoggerFactory.getLogger(ImportServiceHandler.class);

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private StatusRuleService statusRuleService;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public ImportResult doImport(Community community, InputStream inputStream) {

        long startTime = System.currentTimeMillis();

        // delete all subs and members for community
        Collection<Subscription> currentSubs = subscriptionRepository.findByCommunity(community);
        List<Member> membersToDelete = new ArrayList<>();

        for (Subscription currentSub : currentSubs) {
            membersToDelete.add(currentSub.getMember());// TODO do we need to check for multiple subscriptions?
        }

        subscriptionRepository.delete(currentSubs);
        memberRepository.delete(membersToDelete);

        CsvLoader csvLoader = findLoaderForCommunity(community);

        ImportResult result = csvLoader.load(inputStream, community.getIdNumber());

        log.info(String.format("Success=%d, problems=%d, duration=%d ms", result.getSuccessCount(), result.getIssues().size(),
                System.currentTimeMillis() - startTime));

        log.debug("Import result: " + result);
        for (ImportRowIssue issue : result.getIssues()) {
            log.debug("Issue: " + issue);
        }

        try {
            statusRuleService.runRulesForCommunity(community);
        } catch (Exception e) {
            log.error("Problem: " + e);
        }

        return result;
    }


    private CsvLoader findLoaderForCommunity(Community community) {
        for (CsvLoader candidate : BeanFactoryUtils.beansOfTypeIncludingAncestors(
                applicationContext, CsvLoader.class).values()) {
            if (candidate.supports(community.getName())) {
                return candidate;
            }
        }

        return applicationContext.getBean("defaultCsvLoader", DefaultCsvLoader.class);
    }
}
