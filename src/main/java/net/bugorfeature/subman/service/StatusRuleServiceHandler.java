/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.StatusRule;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.MemberRepository;
import net.bugorfeature.subman.repo.StatusRuleRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;

/**
 * Status rules service implementation
 *
 * @author Andy Geach
 */
@Service
public class StatusRuleServiceHandler implements StatusRuleService {

    private static final Logger log = LoggerFactory.getLogger(StatusRuleServiceHandler.class);

    @Autowired
    private CommunityRepository communityRepo;

    @Autowired
    private SubscriptionRepository subsRepo;

    @Autowired
    private MemberRepository memberRepo;

    @Autowired
    private StatusRuleRepository ruleRepo;

    /**
     * Runs the rules every day just past midnight and updates subscription status
     * according to days since payment due
     *
     * @Scheduled(cron="0 * 15 * * * *") // every 15 minutes
     * @Scheduled(fixedRate = 20000) // every 20 seconds
     */
    @Scheduled(cron = "0 1 0 * * *") // 1 min past midnight every day
    @Override
    public void runAllRules() {

        log.info("Running status rules");

        for (Community community : communityRepo.findAll()) {
            Iterable<StatusRule> rules = ruleRepo.findByCommunityOrderByDayCountAsc(community);
            for (Subscription sub : subsRepo.findByCommunity(community)) {
                runRules(community, rules, sub);
            }
        }
    }

    @Override
    public void runRulesForCommunity(Community community) {
        Iterable<StatusRule> rules = ruleRepo.findByCommunityOrderByDayCountAsc(community);
        for (Subscription sub : subsRepo.findByCommunity(community)) {
            runRules(community, rules, sub);
        }
    }

    private void runRules(Community community, Iterable<StatusRule> rules, Subscription sub) {
        int count = Days.daysBetween(sub.getDueDate(), new LocalDate()).getDays();
        StatusRule rule = findRule(count, rules);
        if (rule != null && !sub.getStatus().equals(rule.getStatus())) {
            Member member = memberRepo.findBySubscriptionsIdNumber(sub.getIdNumber());
            log.info(String.format("Updating Subscription for '%s %s' of Community '%s' to '%s' as it is " +
                            "%d days overdue", member.getFirstName(), member.getLastName(), community.getName(),
                    rule.getStatus().getDescription(), count));
            sub.setStatus(rule.getStatus());
            subsRepo.save(sub);
        }
    }

    private StatusRule findRule(int count, Iterable<StatusRule> rules) {
        StatusRule result = null;
        for (StatusRule rule : rules) {
            if (count >= rule.getDayCount()) {
                result = rule;
            }
        }
        return result;
    }
}
