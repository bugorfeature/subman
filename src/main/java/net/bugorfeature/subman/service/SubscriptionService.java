/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import java.util.List;
import java.util.Map;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Subscription;

/**
 * Subscription service
 *
 * @author Andy Geach
 */
public interface SubscriptionService {

    public Iterable<Subscription> getSubscribersForCommunity(Community community);

    public Subscription getSubscription(Integer memberId);

    public Subscription getSubscription(Integer memberId, Integer communityId);

    public void save(Subscription subscription);

    public List<Map<String, String>> getStatusStats(Community community);

    public List<Map<String, String>> getFrequencyStats(Community community);

    public List<Map<String, String>> getPaymentTypeStats(Community community);
}
