/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.repo.CommunityRepository;

/**
 * Community service implementation
 *
 * @author Andy Geach
 */
@Service
public class CommunityServiceHandler implements CommunityService {

    @Autowired
    private CommunityRepository communityRepository;

    @Override
    public Community getCommunityForId(Integer id) {
        return communityRepository.findOne(id);
    }

    @Override
    public List<Community> getCommunitiesForUsername(String username) {
        return communityRepository.findByUsersUsername(username);
    }

    public CommunityRepository getCommunityRepository() {
        return communityRepository;
    }

    public void setCommunityRepository(
        CommunityRepository communityRepository) {
        this.communityRepository = communityRepository;
    }
}
