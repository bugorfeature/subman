/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.Map;

import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.springframework.util.Assert;

/**
 * Gender enumeration
 *
 * @author Andy Geach
 */
public enum Gender {

    M("M"),
    F("F");

    private static final Map<String, Gender> stringToEnum = new CaseInsensitiveMap<>();

    static {
        for (Gender gender : values()) {
            stringToEnum.put(gender.description, gender);
        }
    }

    private String description;

    Gender(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static Gender fromString(String description) {
        Gender result = stringToEnum.get(description);
        Assert.notNull(result, String.format("Invalid value for Gender: %s", description));
        return result;
    }
}
