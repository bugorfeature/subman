/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.Map;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.collections4.map.CaseInsensitiveMap;

/**
 * Represents possible values for subscription status
 *
 * TODO refactor status rules for each community
 *
 * @author Andy Geach
 */
public enum SubscriptionStatus {

    /*
    Current =  payment falls within the last 365 days
    Lapsed =  payment is between 1 and 31 days late
    Expired =  payment is between 32 days and 91 days late
    Inactive = payment is over 91 days late
     */

    CURRENT("Current"),
    EXPIRED("Expired"),
    INACTIVE("Inactive"),
    LAPSED("Lapsed");

    private String description;

    SubscriptionStatus(String description) {
        this.description = description;
    }

    private static final Map<String, SubscriptionStatus> stringToEnum = new CaseInsensitiveMap<>();

    static {
        for (SubscriptionStatus value : values()) {
            stringToEnum.put(value.description, value);
        }
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

    public static SubscriptionStatus fromString(String description) {
        SubscriptionStatus result = stringToEnum.get(description);
        Assert.notNull(result, String.format("Invalid value for SubscriptionStatus: %s", description));
        return result;
    }
}
