/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Entity representing an organisation, club or association for which membership can be managed
 *
 * @author Andy Geach
 */
@Entity(name = "community")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "users", "statusRules"})
public class Community implements Serializable {

    private static final long serialVersionUID = 4756683859904233155L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "COMMUNITY_ID")
    private Integer idNumber;

    @Column(nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "community")
    @JsonBackReference
    private Set<Subscription> subscriptions = new HashSet<>();

    public void addSubscription(Subscription subscription) {
        this.subscriptions.add(subscription);
        subscription.setCommunity(this);
    }

    @OneToMany(mappedBy = "community")
    private Set<StatusRule> statusRules = new HashSet<>();

    @ManyToMany(mappedBy="communities")
    private Set<User> users = new HashSet<>();

    public void addStatusRule(StatusRule rule) {
        this.statusRules.add(rule);
        rule.setCommunity(this);
    }

    @Column(name = "ABOUT")
    private String about;


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("idNumber", idNumber)
                .append("name", name)
                .append("subscriptions", subscriptions)
                .append("statusRules", statusRules)
                .append("users", users)
                .append("about", about)
                .toString();
    }

    public Set<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Set<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<StatusRule> getStatusRules() {
        return statusRules;
    }

    public void setStatusRules(Set<StatusRule> statusRules) {
        this.statusRules = statusRules;
    }
}
