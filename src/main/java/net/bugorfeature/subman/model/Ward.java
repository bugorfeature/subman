/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.Map;

import org.springframework.util.Assert;

import org.apache.commons.collections4.map.CaseInsensitiveMap;

/**
 * Represents possible Wards
 *
 *
 * @author Andy Geach
 */
public enum Ward {

    ABBEYGATE("Abbeygate"),
    BACTON_OLD_NEWTON("Bacton and Old Newton"),
    BADWELL_ASH("Badwell Ash"),
    EASTGATE("Eastgate"),
    ELMSWELL_NORTON("Elmswell and Norton"),
    EX_CONSTITUENCY("Ex-Constituency"),
    FORNHAM("Fornham"),
    GISLINGHAM("Gislingham"),
    GREAT_BARTON("Great Barton"),
    HAUGHLEY_WETHERDEN("Haughley and Wetherden"),
    HORRINGER_AND_WHELNETHAM("Horringer and Whelnetham"),
    MINDEN("Minden"),
    MORETON_HALL("Moreton Hall"),
    NEEDHAM_MARKET("Needham Market"),
    NORTHGATE("Northgate"),
    ONEHOUSE("Onehouse"),
    PAKENHAM("Pakenham"),
    RATTLESDEN("Rattlesden"),
    RICKINGHALL_AND_WALSHAM("Rickinghall and Walsham"),
    RINGSHALL("Ringshall"),
    RISBYGATE("Risbygate"),
    ROUGHAM("Rougham"),
    SOUTHGATE("Southgate"),
    ST_OLAVES("St Olaves"),
    STOWMARKET_CENTRAL("Stowmarket Central"),
    STOWMARKET_NORTH("Stowmarket North"),
    STOWMARKET_SOUTH("Stowmarket South"),
    STOWUPLAND("Stowupland"),
    THURSTON_HESSETT("Thurston and Hessett"),
    WESTGATE("Westgate"),
    WOOLPIT("Woolpit");

    private String description;

    Ward(String name) {
        this.description = name;
    }

    private static final Map<String, Ward> stringToEnum = new CaseInsensitiveMap<>();

    static {
        for (Ward value : values()) {
            stringToEnum.put(value.description, value);
        }
    }

    public String getDescription() {
        return description;
    }

    public static Ward fromString(String description) {
        Ward result = stringToEnum.get(description);
        Assert.notNull(result, String.format("Invalid value for Ward: %s", description));
        return result;
    }
}
