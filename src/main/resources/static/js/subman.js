// common functions

$(document).ready(function () {
    setupMemberTable();
    setActiveMenu();

    $("#cancelEditButton").click(function() {
        window.location.href = "/community/listMembers"
    });

    $("#paidDate").datepicker({
        todayBtn: "linked",
        format: 'yyyy-mm-dd',
        autoclose: true}).on("changeDate",
        function(newDate) {
            updateFields(newDate.date);
        }
    );

    $("#dueDate").datepicker({
        todayBtn: "linked",
        format: 'yyyy-mm-dd',
        autoclose: true}
    );
});

/*
SubscriptionStatus:
Current =  payment falls within the last 365 days
Lapsed =  payment is between 1 and 31 days late
Expired =  payment is between 32 days and 91 days late
Inactive = payment is over 91 days late
 */
function updateFields(d) {
    var freq = $("#frequency").val();
    //console.log(d);
    //console.log(freq);
    var currentDueDate = $("#originalDueDate").val();
    var newDueDate;
    //var paidDate = moment(d, "YYYY-MM-DD");
    var paidDate = moment(d);
    //console.log(paidDate.format());

    var count = moment().diff(paidDate, 'days'); // count in days between paidDate and now
    //console.log(count);

    var frequencyInDays;

    if (freq == "ANNUAL") {
        newDueDate = moment(currentDueDate, "YYYY-MM-DD").add(1, 'y').format("YYYY-MM-DD");
        frequencyInDays = 365;

    } else if (freq == "BI_ANNUAL") {
        newDueDate = moment(currentDueDate, "YYYY-MM-DD").add(6, 'M').format("YYYY-MM-DD");
        frequencyInDays = 183;

    } else if (freq == "QUARTER") {
        newDueDate = moment(currentDueDate, "YYYY-MM-DD").add(3, 'M').format("YYYY-MM-DD");
        frequencyInDays = 91;

    } else if (freq == "MONTH") {
        newDueDate = moment(currentDueDate, "YYYY-MM-DD").add(1, 'M').format("YYYY-MM-DD");
        frequencyInDays = 31;
    }

    var status = "INACTIVE";
    if (count < (frequencyInDays + 91)) {
        status = "EXPIRED";
    }
    if (count < (frequencyInDays + 32)) {
        status = "LAPSED";
    }
    if (count < (frequencyInDays + 1)) {
        status = "CURRENT";
    }

    //console.log(count + ', ' + status);
    //console.log('ndd: ' + newDueDate);
    $("#dueDate").val(newDueDate);
    $("#statusField").val(status);
}

// set up list view table
function setupMemberTable() {
    $.fn.dataTable.moment( 'YYYY-MM-DD' ); // TODO options
    var table = $('#members').dataTable({
        "order": [ 6, 'asc' ],
        "pageLength": 10,
        "lengthMenu": [ 10, 15, 20, 30, 40, 50, 75, 100 ],
        "ajax": {
            "url": "/api/subscriberList",
            "dataSrc": ""
        },
        "columns": [
            { "data": "member.firstName" },
            { "data": "member.lastName" },
            { "data": "paymentMethod", "searchable": false },
            { "data": "frequency", "searchable": false },
            { "data": "dueAmount", "searchable": false , render: $.fn.dataTable.render.number( ',', '.', 2, '£' ) },
            { "data": "dueDate", "searchable": false  },
            { "data": "status", "searchable": false  },
            { "data": "member.idNumber",
                orderable: false,
                "render": function (data, type, full, meta) {
                    return buildEditButton(data);
                }
            }
        ]
    });

    if (table.length) {
        var tt = new $.fn.dataTable.TableTools(table, {
            "aButtons": [
                {
                    "sExtends": "text",
                    "sButtonText": "Excel",
                    "fnClick": function (nButton, oConfig, oFlash) {
                        window.location.href = "/doc/xls"
                    }
                },
                {
                    "sExtends": "text",
                    "sButtonText": "PDF",
                    "fnClick": function (nButton, oConfig, oFlash) {
                        window.location.href = "/doc/pdf"
                    }
                }
            ]
        });
        $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');
    }
}

function buildEditButton(id) {
    return "<div class='btn-group'>" +
        "<button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>More " +
        "<span class='caret'></span></button>" +
        "<ul class='dropdown-menu' role='menu'>" +
        "<li><a href='/subscription/pay/" + id + "'>Add Payment</a></li>" +
        "<li><a href='/member/edit/" + id + "'>Edit Member details</a></li>" +
        "<li><a href='/subscription/edit/" + id + "'>Edit Subscription details</a></li>" +
        "</ul></div>"
}

function buildDateFromLong(data, type, full, meta) {
    var theDate = new Date();
    theDate.setTime(data);
    return $.datepicker.formatDate('dd/mm/yy', theDate)
}

function buildBetterDateFromLong(data, type, full, meta) {
    var theDate = moment(data);
    return theDate.format('YYYY-MM-DD')
}

function doLogout() {
    $('#logout-form').submit()
}

function setActiveMenu() {
    var myUrl = window.location.pathname;
    $('ul.nav a[href="'+ myUrl +'"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    //$('ul.nav a').filter(function() {
    //    return this.href == url;
    //}).parent().addClass('active');
}

