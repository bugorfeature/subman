DROP TABLE IF EXISTS persistent_login;
DROP TABLE IF EXISTS persistent_logins;
DROP TABLE IF EXISTS authority;
DROP TABLE IF EXISTS user_community;
DROP TABLE IF EXISTS user_table;
DROP TABLE IF EXISTS subscription;
DROP TABLE IF EXISTS member;
DROP TABLE IF EXISTS status_rule;
DROP TABLE IF EXISTS community;
DROP TABLE IF EXISTS member_type;
DROP TABLE IF EXISTS membership_type;

DROP TABLE IF EXISTS schema_version;

