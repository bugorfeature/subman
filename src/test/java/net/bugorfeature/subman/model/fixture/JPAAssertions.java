/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model.fixture;

import static org.junit.Assert.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;
import org.hibernate.jdbc.Work;

import javax.persistence.EntityManager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Utility class for helping to test JPA mappings
 *
 */
public class JPAAssertions {

    private static final Logger log = LoggerFactory.getLogger(JPAAssertions.class);

    /**
     *
     * @param manager
     * @param tableName
     * @param columnName
     */
    public static void assertTableHasColumn(EntityManager manager, final String tableName, final String columnName) {
        SessionImpl session = (SessionImpl) manager.unwrap(Session.class);

        final ResultCollector rc = new ResultCollector();

        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                ResultSet columns = connection.getMetaData().getColumns(null, null, tableName.toUpperCase(), null);
                while (columns.next()) {

                    String colName = columns.getString(4).toUpperCase();
                    log.trace("Column name: " + colName);

                    if (colName.equals(columnName.toUpperCase())) {
                        rc.found = true;
                    }
                }
            }
        });

        if (!rc.found) {
            fail("Column [" + columnName + "] not found on table : " + tableName);
        }
    }

    /**
     *
     * @param manager
     * @param name
     */
    public static void assertTableExists(EntityManager manager, final String name) {
        SessionImpl session = (SessionImpl) manager.unwrap(Session.class);

        final ResultCollector rc = new ResultCollector();

        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                ResultSet tables = connection.getMetaData().getTables(null, null, "%", null);
                while (tables.next()) {
                    String tableName = tables.getString(3).toUpperCase();
                    log.trace("Table name: " + tableName);

                    if (tableName.equals(name.toUpperCase())) {
                        rc.found = true;
                    }
                }
            }
        });

        if (!rc.found) {
            fail("Table not found in schema : " + name);
        }
    }

    static class ResultCollector {
        public boolean found = false;
    }
}
