/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.UUID;

import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.builder.SubscriptionBuilder;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.MemberRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;
import net.bugorfeature.subman.util.Utils;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Integration test for relationships
 *
 * @author Andy Geach
 */
public class RelationshipsITCase extends AbstractMappingITCase {

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    CommunityRepository communityRepository;

    @Test
    public void testRelations() {

        String telephoneNo = "01284 321321";
        Member member = MemberBuilder.aMember().build();
        member.setTelephoneNo(telephoneNo);
        member = memberRepository.save(member);

        Community community = CommunityBuilder.aCommunity().build();
        community.setName("Garden Club");
        community = communityRepository.save(community);

        String number = UUID.randomUUID().toString();
        Subscription subscription = SubscriptionBuilder.aSubscription().build();
        subscription.setNumber(number);
        subscription.setDueDate(Utils.buildDate("2014-07-18"));
        subscription.setPaidDate(Utils.buildDate("2014-07-17"));
        member.addSubscription(subscription);
        community.addSubscription(subscription);
        subscription = subscriptionRepository.save(subscription);

        Subscription retrieved = subscriptionRepository.findByMemberIdNumber(member.getIdNumber());

        assertNotNull(retrieved);

        assertTrue(retrieved.getCommunity().getSubscriptions().size() > 0);
        assertTrue(retrieved.getMember().getSubscriptions().size() > 0);
    }

    @Ignore
    @Test
    public void testLoadAll() {
        assertThat(communityRepository.findAll(), IsIterableWithSize.<Community>iterableWithSize(2));
        assertThat(memberRepository.findAll(), IsIterableWithSize.<Member>iterableWithSize(18));
        assertThat(subscriptionRepository.findAll(), IsIterableWithSize.<Subscription>iterableWithSize(18));
    }
}
