/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import org.junit.Test;

import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration test for member controller
 *
 * @author Andy Geach
 */
public class MemberControllerITCase extends AbstractControllerITCase {

    @Test
    public void newMember() throws Exception {
        mockMvc.perform(get("/member/new"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("commPrefsTypes", "salutations", "genders"))
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("createOrEditMember"))
                .andExpect(xpath("//select[@id='customField_1']").doesNotExist());
    }

    @Test
    public void newBseccaMember() throws Exception {
        mockMvc.perform(get("/member/new").sessionAttr("communityName", "BSECCA"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("commPrefsTypes", "salutations", "genders"))
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("createOrEditMember"))
                .andExpect(xpath("//select[@id='customField_1']").exists());
    }

    @Test
    public void setupEditInvalid() throws Exception {
        when(memberService.getMember(anyInt(), anyInt())).thenReturn(null);
        mockMvc.perform(get("/member/edit/{memberId}", memberId).sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(flash().attributeExists("warning"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }

    @Test
    public void setupEditValid() throws Exception {
        when(memberService.getMember(anyInt(), anyInt())).thenReturn(member);
        mockMvc.perform(get("/member/edit/{memberId}", memberId).sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(model().attributeExists("member", "commPrefsTypes", "salutations", "genders"))
                .andExpect(status().isOk())
                .andExpect(view().name("createOrEditMember"));
    }


    @Test
    public void submitInvalid() throws Exception {
        mockMvc.perform(post("/member/submit").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community).with(csrf())
                .param("firstName", "blah"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("createOrEditMember"));
    }

    @Test
    public void submitValidUpdate() throws Exception {
        when(memberService.getMember(anyInt(), anyInt())).thenReturn(member);
        mockMvc.perform(post("/member/submit").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community).with(csrf())
                .param("idNumber", "1233")
                .param("salutation", "MR")
                .param("firstName", "John")
                .param("lastName", "Johnson")
                .param("gender", "M")
                .param("address1", "4 Harris Road")
                .param("townCity", "Sevenoaks")
                .param("county", "Kent")
                .param("postCode", "SV1 4FF")
                .param("emailAddress", "john@google.com")
                .param("commPrefs", "NONE"))
                .andDo(print())
                .andExpect(flash().attribute("message", endsWith("saved successfully")))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }

    @Test
    public void submitValidNew() throws Exception {
        mockMvc.perform(post("/member/submit").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community).with(csrf())
                .param("salutation", "MR")
                .param("firstName", "John")
                .param("lastName", "Johnson")
                .param("gender", "M")
                .param("address1", "4 Harris Road")
                .param("townCity", "Sevenoaks")
                .param("county", "Kent")
                .param("postCode", "SV1 4FF")
                .param("emailAddress", "john@google.com")
                .param("commPrefs", "NONE"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/subscription/new"));
    }
}
