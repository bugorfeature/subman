/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.repo;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.builder.SubscriptionBuilder;
import net.bugorfeature.subman.model.Branch;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.util.Utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Subscription repo integration test
 *
 * @author Andy Geach
 */
public class SubscriptionRepositoryITCase extends AbstractRepositoryITCase {

    private static final Logger log = LoggerFactory.getLogger(SubscriptionRepositoryITCase.class);

    @Autowired
    SubscriptionRepository repository;

    @Autowired
    CommunityRepository communityRepository;

    @Autowired
    MemberRepository memberRepository;

    @Test
    public void testInsert() {

        Community community = new CommunityBuilder().build();
        community = communityRepository.save(community);

        Member member = new MemberBuilder().build();
        member = memberRepository.save(member);

        String number = UUID.randomUUID().toString();

        Subscription instance = SubscriptionBuilder.aSubscription().build();
        instance.setCommunity(community);
        instance.setMember(member);
        instance.setNumber(number);
        instance.setDueDate(Utils.buildDate("2016-01-01"));
        instance.setPaidDate(Utils.buildDate("2016-01-01"));

        log.info("Subscription instance: " + instance.toString());

        instance = repository.save(instance);

        Subscription retrieved = repository.findOne(instance.getIdNumber());

        assertNotNull(retrieved);
        assertEquals(number, retrieved.getNumber());
        assertEquals(Branch.BURY_TOWN.name(), retrieved.getCustomField_1());
        assertTrue(retrieved.getCardIssued());
        assertEquals(new BigDecimal("25.00"), retrieved.getDueAmount());
    }

    @Test
    public void testStatusStats() {
        List<Object[]> retrieved = repository.getStatusStats(communityRepository.findOne(3));
        assertNotNull(retrieved);
    }

    @Test
    public void testBranchStats() {
        List<Object[]> retrieved = repository.getBranchStats(communityRepository.findOne(3));
        assertNotNull(retrieved);
    }

    @Test
    public void testFrequencyStats() {
        List<Object[]> retrieved = repository.getFrequencyStats(communityRepository.findOne(3));
        assertNotNull(retrieved);
    }

    @Test
    public void testPaymentTypeStats() {
        List<Object[]> retrieved = repository.getPaymentTypeStats(communityRepository.findOne(3));
        assertNotNull(retrieved);
    }

    @Test
    public void testWardStats() {
        List<Object[]> retrieved = repository.getWardStats(communityRepository.findOne(3));
        assertNotNull(retrieved);
    }
}
