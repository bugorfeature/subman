/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.repo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.builder.StatusRuleBuilder;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.StatusRule;
import net.bugorfeature.subman.model.SubscriptionStatus;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Status rule repo integration test
 *
 * @author Andy Geach
 */
public class StatusRuleRepositoryITCase extends AbstractRepositoryITCase {

    private static final Logger log = LoggerFactory.getLogger(StatusRuleRepositoryITCase.class);

    @Autowired
    private StatusRuleRepository repository;

    @Autowired
    private CommunityRepository communityRepo;


    @Test
    public void testInsert() {

        Community community = CommunityBuilder.aCommunity().name("blah").build();
        communityRepo.save(community);

        StatusRule instance = StatusRuleBuilder.aStatusRule().status(SubscriptionStatus.CURRENT).community(community).dayCount(1).build();
        instance = repository.save(instance);

        StatusRule retrieved = repository.findOne(instance.getIdNumber());

        assertNotNull(retrieved);
    }

    @Test
    public void testFind() {

        Community community = CommunityBuilder.aCommunity().name("blah").build();
        communityRepo.save(community);

        StatusRule two = StatusRuleBuilder.aStatusRule().status(SubscriptionStatus.EXPIRED).community(community).dayCount(2).build();
        two = repository.save(two);

        StatusRule one = StatusRuleBuilder.aStatusRule().status(SubscriptionStatus.LAPSED).community(community).dayCount(1).build();
        one = repository.save(one);

        StatusRule three = StatusRuleBuilder.aStatusRule().status(SubscriptionStatus.INACTIVE).community(community).dayCount(3).build();
        three = repository.save(three);

        Collection<StatusRule> coll = repository.findByCommunityOrderByDayCountAsc(community);
        List<StatusRule> list = new ArrayList<>();
        list.addAll(coll);

        assertTrue(list.get(2).getDayCount() == 3);
        assertTrue(list.get(2).getStatus() == SubscriptionStatus.INACTIVE);

        assertTrue(list.get(1).getDayCount() == 2);
        assertTrue(list.get(1).getStatus() == SubscriptionStatus.EXPIRED);

        assertTrue(list.get(0).getDayCount() == 1);
        assertTrue(list.get(0).getStatus() == SubscriptionStatus.LAPSED);
    }
}
