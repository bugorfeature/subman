/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import submanx.ITCaseConfig;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.builder.StatusRuleBuilder;
import net.bugorfeature.subman.builder.SubscriptionBuilder;
import net.bugorfeature.subman.config.CsvLoaderConfig;
import net.bugorfeature.subman.config.ValidationConfig;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.StatusRule;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.MemberRepository;
import net.bugorfeature.subman.repo.StatusRuleRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;

import static org.junit.Assert.assertTrue;

/**
 * Integration test for the Status Rule service
 *
 * @author Andy Geach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {
        ITCaseConfig.class,
        CsvLoaderConfig.class,
        ValidationConfig.class
})
@Transactional
@WebAppConfiguration
@ActiveProfiles("itcase")
public class StatusRuleServiceITCase {

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    CommunityRepository communityRepository;

    @Autowired
    StatusRuleRepository ruleRepository;

    @Autowired
    StatusRuleService service;

    Community community;
    Subscription subOne;
    Subscription subTwo;
    Subscription subThree;

    @Test
    public void runAll() {

        assertTrue(subOne.getStatus().equals(SubscriptionStatus.CURRENT));
        assertTrue(subTwo.getStatus().equals(SubscriptionStatus.CURRENT));
        assertTrue(subThree.getStatus().equals(SubscriptionStatus.CURRENT));

        service.runAllRules();

        assertTrue(subOne.getStatus().equals(SubscriptionStatus.CURRENT));
        assertTrue(subTwo.getStatus().equals(SubscriptionStatus.LAPSED));
        assertTrue(subThree.getStatus().equals(SubscriptionStatus.EXPIRED));
    }

    @Test
    public void runAllForCommunity() {

        assertTrue(subOne.getStatus().equals(SubscriptionStatus.CURRENT));
        assertTrue(subTwo.getStatus().equals(SubscriptionStatus.CURRENT));
        assertTrue(subThree.getStatus().equals(SubscriptionStatus.CURRENT));

        service.runRulesForCommunity(community);

        assertTrue(subOne.getStatus().equals(SubscriptionStatus.CURRENT));
        assertTrue(subTwo.getStatus().equals(SubscriptionStatus.LAPSED));
        assertTrue(subThree.getStatus().equals(SubscriptionStatus.EXPIRED));
    }

    @Before
    public void setup() {

        community = CommunityBuilder.aCommunity().build();
        communityRepository.save(community);

        Member memberOne = MemberBuilder.aMember().build();
        Member memberTwo = MemberBuilder.aMember().build();
        Member memberThree = MemberBuilder.aMember().build();

        memberRepository.save(memberOne);
        memberRepository.save(memberTwo);
        memberRepository.save(memberThree);

        subOne = SubscriptionBuilder.aSubscription().community(community).member(memberOne).number("1").dueDate(new LocalDate().minusDays(1)).build();
        subTwo = SubscriptionBuilder.aSubscription().community(community).member(memberTwo).number("2").dueDate(new LocalDate().minusDays(3)).build();
        subThree = SubscriptionBuilder.aSubscription().community(community).member(memberThree).number("3").dueDate(new LocalDate().minusDays(5)).build();

        community.addSubscription(subOne);
        community.addSubscription(subTwo);
        community.addSubscription(subThree);

        subscriptionRepository.save(subOne);
        subscriptionRepository.save(subTwo);
        subscriptionRepository.save(subThree);

        StatusRule ruleOne = StatusRuleBuilder.aStatusRule().community(community).dayCount(1).status(SubscriptionStatus.CURRENT).build();
        StatusRule ruleTwo = StatusRuleBuilder.aStatusRule().community(community).dayCount(3).status(SubscriptionStatus.LAPSED).build();
        StatusRule ruleThree = StatusRuleBuilder.aStatusRule().community(community).dayCount(5).status(SubscriptionStatus.EXPIRED).build();

        community.addStatusRule(ruleOne);
        community.addStatusRule(ruleTwo);
        community.addStatusRule(ruleThree);

        ruleRepository.save(ruleOne);
        ruleRepository.save(ruleTwo);
        ruleRepository.save(ruleThree);
    }
}
