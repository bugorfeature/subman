/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import submanx.ITCaseConfig;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.config.CsvLoaderConfig;
import net.bugorfeature.subman.config.ValidationConfig;
import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;

import static org.junit.Assert.assertTrue;


/**
 * Integration test for default CSV Loader
 *
 * @author Andy Geach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {
        ITCaseConfig.class,
        CsvLoaderConfig.class,
        ValidationConfig.class
})
@Transactional
@WebAppConfiguration
@ActiveProfiles("itcase")
public class CsvLoaderITCase {

    private static final Logger log = LoggerFactory.getLogger(CsvLoaderITCase.class);

    @Autowired
    protected CommunityRepository communityRepository;

    @Autowired
    protected SubscriptionRepository subscriptionRepository;

    @Autowired
    @Qualifier(value = "defaultCsvLoader")
    private Loader csvLoader;

    @Value("classpath:data/hhcc_example.csv")
    private Resource inputFile;

    @Value("classpath:data/hhcc_bad_member.csv")
    private Resource badMemberFile;

    @Value("classpath:data/hhcc_null_member.csv")
    private Resource nullMemberFile;

    @Test
    public void happyDays() throws IOException {

        Community comm = CommunityBuilder.aCommunity().build();
        comm.setName("Test Community");
        comm = communityRepository.save(comm);

        ImportResult result = csvLoader.load(inputFile.getInputStream(), comm.getIdNumber());

        logIssues(result);

        assertTrue("No issues expected", result.getIssues().size() == 0);
        assertTrue("Expected 9 successes", result.getSuccessCount() == 9);

        log.info("Expected: 2014-01-01, actual: " + subscriptionRepository.findByNumber("HHCC1").getDueDate());

        assertTrue(subscriptionRepository.findByNumber("HHCC1").getDueDate().getYear() == 2014);
        assertTrue(subscriptionRepository.findByNumber("HHCC1").getDueDate().getMonthOfYear() == 12);
        assertTrue(subscriptionRepository.findByNumber("HHCC1").getDueDate().getDayOfMonth() == 1);
        assertTrue(subscriptionRepository.findByNumber("HHCC1").getMember().getCommPrefs() == CommunicationPreferencesType.NONE);

        assertTrue(comm.getUsers().size() == 0);
        assertTrue(comm.getStatusRules().size() == 0);
    }

    @Test
    public void badMember() throws IOException {

        Community comm = CommunityBuilder.aCommunity().build();
        comm.setName("Test Community");
        comm = communityRepository.save(comm);

        ImportResult result = csvLoader.load(badMemberFile.getInputStream(), comm.getIdNumber());

        logIssues(result);

        assertTrue(result.getIssues().size() == 1);
        assertTrue(result.get(0).getColName().equals("subscriptionType"));
        assertTrue(result.get(0).getMessage().endsWith("could not be parsed as a enum of type net.bugorfeature.subman.model.SubscriptionType"));
        assertTrue(result.get(0).getRowNum() == 2);
        assertTrue(result.get(0).getColNum() == 20);
        assertTrue(result.getSuccessCount() == 0);
    }

    @Test
    public void badCommunity() throws IOException {

        ImportResult result = csvLoader.load(inputFile.getInputStream(), 999);

        logIssues(result);

        assertTrue(result.getIssues().size() == 1);
        assertTrue(result.getSuccessCount() == 0);
    }

    @Test
    public void nullMember() throws IOException {

        Community comm = CommunityBuilder.aCommunity().build();
        comm.setName("Test Community");
        comm = communityRepository.save(comm);

        ImportResult result = csvLoader.load(nullMemberFile.getInputStream(), comm.getIdNumber());

        logIssues(result);

        assertTrue(result.getIssues().size() == 1);
        assertTrue(result.getSuccessCount() == 0);
    }

    @Test
    public void badfile() throws IOException {

        Community comm = CommunityBuilder.aCommunity().build();
        comm.setName("Test Community");
        comm = communityRepository.save(comm);

        ImportResult result = csvLoader.load(null, comm.getIdNumber());

        logIssues(result);

        assertTrue(result.getSuccessCount() == 0);
    }

    private void logIssues(ImportResult result) {
        if (result != null && result.getIssues().size() != 0) {
            log.info("Import result: " + result);
            for (ImportRowIssue issue : result.getIssues()) {
                log.info("Issue: " + issue);
            }
        }
    }
}
