/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import submanx.ITCaseConfig;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.config.CsvLoaderConfig;
import net.bugorfeature.subman.config.ValidationConfig;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;

import static org.junit.Assert.assertTrue;


/**
 * Integration test for BSECCA CSV Loader
 *
 * @author Andy Geach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {
        ITCaseConfig.class,
        CsvLoaderConfig.class,
        ValidationConfig.class
})
@WebAppConfiguration
@Transactional
@ActiveProfiles("itcase")
public class BseccaLoaderITCase {

    private static final Logger log = LoggerFactory.getLogger(BseccaLoaderITCase.class);

    @Autowired
    protected CommunityRepository communityRepository;

    @Autowired
    protected SubscriptionRepository subscriptionRepository;

    @Autowired
    @Qualifier(value = "bseccaCsvLoader")
    private Loader csvLoader;

    @Value("classpath:data/bsecca_example.csv")
    private Resource inputFile;

    @Test
    public void test() {

        Community comm = CommunityBuilder.aCommunity().name("Test Community").build();
        comm = communityRepository.save(comm);

        ImportResult result = null;
        try {
            result = csvLoader.load(inputFile.getInputStream(), comm.getIdNumber());
        } catch (IOException e) {
            log.error("Problem: " + e);
        }

        if (result != null && result.getIssues().size() != 0) {
            log.info("Import result: " + result);
            for (ImportRowIssue issue : result.getIssues()) {
                log.info("Issue: " + issue);
            }
        }

        assertTrue("No issues expected", result.getIssues().size() == 0);
        assertTrue("Expected 9 successes", result.getSuccessCount() == 9);
    }
}
