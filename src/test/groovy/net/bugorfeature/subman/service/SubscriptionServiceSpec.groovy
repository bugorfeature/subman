/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service

import net.bugorfeature.subman.model.PaymentMethod
import net.bugorfeature.subman.model.SubscriptionFrequency
import net.bugorfeature.subman.model.SubscriptionStatus
import net.bugorfeature.subman.repo.CommunityRepository
import net.bugorfeature.subman.repo.SubscriptionRepository
import spock.lang.Specification
/**
 * Specification for SubscriptionService
 *
 * @author Andy Geach
 */
class SubscriptionServiceSpec extends Specification {

    SubscriptionServiceHandler service
    CommunityRepository mockCommunityRepo
    SubscriptionRepository mockSubscriptionRepo
    def result

    def setup() {

        service = new SubscriptionServiceHandler()
        mockSubscriptionRepo = Mock(SubscriptionRepository)
        mockCommunityRepo = Mock(CommunityRepository)
        service.setSubscriptionRepository(mockSubscriptionRepo)
        service.setCommunityRepository(mockCommunityRepo)
    }


    def "status statistics are calculated correctly"() {

        def currentCount = [SubscriptionStatus.CURRENT, 21L] as Object[]
        def expiredCount = [SubscriptionStatus.EXPIRED, 5L] as Object[]
        mockSubscriptionRepo.getStatusStats(_) >> [currentCount, expiredCount]

        when:
            def result = service.getStatusStats()

        then:
            result[0].get("value") == "Current"
            result[0].get("count") == "21"

            result[1].get("value") == "Expired"
            result[1].get("count") == "5"

            result[2].get("value") == "Total"
            result[2].get("count") == "26"
    }

    def "frequency statistics are calculated correctly"() {

        def annual = [SubscriptionFrequency.ANNUAL, 2L] as Object[]
        def monthly = [SubscriptionFrequency.MONTH, 7L] as Object[]
        mockSubscriptionRepo.getFrequencyStats(_) >> [annual, monthly]

        when:
            def result = service.getFrequencyStats()

        then:
            result[0].get("value") == "Annually"
            result[0].get("count") == "2"

            result[1].get("value") == "Monthly"
            result[1].get("count") == "7"

            result[2].get("value") == "Total"
            result[2].get("count") == "9"
    }

    def "payment type statistics are calculated correctly"() {

        def cheque = [PaymentMethod.CHEQUE, 52L] as Object[]
        def sorder = [PaymentMethod.STANDING_ORDER, 17L] as Object[]
        mockSubscriptionRepo.getPaymentTypeStats(_) >> [cheque, sorder]

        when:
            def result = service.getPaymentTypeStats()

        then:
            result[0].get("value") == "Cheque"
            result[0].get("count") == "52"

            result[1].get("value") == "Standing order"
            result[1].get("count") == "17"

            result[2].get("value") == "Total"
            result[2].get("count") == "69"
    }
}
