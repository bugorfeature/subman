/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service

import net.bugorfeature.subman.repo.MemberRepository
import net.bugorfeature.subman.repo.SubscriptionRepository
import spock.lang.Specification

/**
 * Specification for ImportService
 *
 * @author Andy Geach
 */
class ImportServiceSpec extends Specification {

    ImportServiceHandler service
    SubscriptionRepository mockSubscriptionRepo
    MemberRepository mockMemberRepo


    def result

    def setup() {

        service = new ImportServiceHandler()
        mockSubscriptionRepo = Mock(SubscriptionRepository)
        mockMemberRepo = Mock(MemberRepository)
        service.setSubscriptionRepository(mockSubscriptionRepo)
        service.setMemberRepository(mockMemberRepo)
    }

    /*def "DoImport"() {
        given:
           1 * mockSubscriptionRepo.findByIdNumberAndSubscriptionsCommunityIdNumber(_, _) >> new Member()

        when:
            result = service.getMember(1, 1)

        then:
            service.getMemberRepository() != null
            assert result != null
            assert result instanceof Member
    }*/
}
