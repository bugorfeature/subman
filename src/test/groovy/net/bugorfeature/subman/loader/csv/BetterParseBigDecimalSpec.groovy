/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader.csv

import org.supercsv.exception.SuperCsvCellProcessorException
import org.supercsv.util.CsvContext
import spock.lang.Specification

import java.text.DecimalFormatSymbols

/**
 * Specification for BetterParseBigDecimal
 *
 * @author Andy Geach
 */
class BetterParseBigDecimalSpec extends Specification {

    BetterParseBigDecimal processor
    DecimalFormatSymbols symbols
    CsvContext context
    BigDecimal result

    def setup() {
        context = new CsvContext(1, 1, 1)
    }

    void 'process BigDecimal values correctly with no currency symbol'() {
        given:
            processor = new BetterParseBigDecimal()

        expect:
            processor.execute("12.50", context) == new BigDecimal("12.50")
    }

    void 'process BigDecimal values correctly with currency symbol'() {
        given:
            symbols = new DecimalFormatSymbols();
            symbols.setCurrencySymbol("£");
            processor = new BetterParseBigDecimal(symbols)

        expect:
            processor.execute("£12.50", context) == new BigDecimal("12.50")
    }

    void 'throw exception on missing Symbol context'() {
        when:
            processor = new BetterParseBigDecimal(null)

        then:
            thrown(NullPointerException)
    }

    void 'throw exception on invalid String input'() {
        given:
            processor = new BetterParseBigDecimal()

        when:
            result = processor.execute("X", context)

        then:
            thrown(SuperCsvCellProcessorException)
    }

    void 'throw exception on non-String input'() {
        given:
            processor = new BetterParseBigDecimal()

        when:
            result = processor.execute(22, context)

        then:
            thrown(SuperCsvCellProcessorException)
    }

}
